import sqlite3 as sql
import re

# Intialize DB
def initDB(fname = 'database.sqlite'):
    db = sql.connect(fname)

    db.execute('''
        CREATE TABLE IF NOT EXISTS people (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(20)
        );''')

    db.execute('''
        CREATE TABLE IF NOT EXISTS products (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(20),
            price INTEGER
        );''')

    db.execute('''
        CREATE TABLE IF NOT EXISTS transactions (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            person INTEGER,
            product INTEGER,
            amount INTEGER,
            FOREIGN KEY (person) REFERENCES people(id),
            FOREIGN KEY (product) REFERENCES products(id)
        );''')
    
    return db

def addPerson(name, cursor = None):
    cursor = cursor or initDB()
    cursor.execute(f"INSERT INTO people VALUES (NULL, \"{name}\")")

def addProduct(name, value, cursor = None):
    cursor = cursor or initDB()
    cursor.execute(f"INSERT INTO products VALUES (NULL, \"{name}\", {value})")

def changePrice(product, value, cursor =None):
    cursor = cursor or initDB()
    cursor.execute(f"UPDATE products SET price={value} WHERE id={product}")

def depositMoney(id, value, cursor = None):
    cursor = cursor or initDB()
    cursor.execute(f"INSERT INTO transactions VALUES (NULL, {id}, NULL, {value})")

def buyProduct(person, product, cursor = None):
    cursor = cursor or initDB()
    cursor.execute(f"INSERT INTO transactions VALUES (NULL, {person}, {product}, -(SELECT price FROM products WHERE products.id = {product}))")

# Formatting
def toMoney(number):
    return f"R$ {number/100}"

def toNumber(string):
    string = string.replace('R', '').replace('$', '').replace(' ', '')
    string = string.replace(',', '.')
    string = string.split('.')

    if len(string[-1]) > 2 or len(string) == 1:
        string[-1] += '00'
    elif len(string[-1]) == 1:
        string[-1] += '0'

    string = ''.join(string)
    return int(string)

# UI
def optionsHelper(config):
    if len(config) == 0:
        print ("No options exist")
        return

    searchResults = config
    
    print()
    while True:
        for index, (name, _) in enumerate(searchResults):
                    print(f"{index + 1}) {name}")
        print()
        
        try:
            raw = input('Escolha uma opção ou texto para pesquisa: ')
            if len(raw) == 0:
                print('Cancelled')
                return

            try:
                option = int(raw)
            except:
                print ('Searching...')
                searchResults = [(name, fun) for name, fun in config if re.match('.*' + raw.lower().replace(' ', '.*') + '.*', name.lower())]
                
            func = searchResults[option - 1][1]

            try:
                func()
            except Exception as e:
                print(e)
            break
        except:
            print("Opção Inválida")


def addPersonUI():
    print()
    print(f"+++++++++++++++++++++++++++++++++")
    print("Adicionar Pessoa")
    print(f"+++++++++++++++++++++++++++++++++")
    print()
    try:
        nome = input("Nome da pessoa: ")
        if len(nome) == 0:
            print('Cancelled')
            return
        money = toNumber(input("Depósito Inicial: "))

        db = initDB()

        cursor = db.cursor()

        addPerson(nome, cursor=cursor)
        depositMoney(cursor.lastrowid, money, cursor=cursor)

        db.commit()
    except Exception as e:
        print(e)
    
def addProductUI():
    print()
    print(f"+++++++++++++++++++++++++++++++++")
    print("Adicionar Produto")
    print(f"+++++++++++++++++++++++++++++++++")
    print()

    try:
        name = input("Nome do produto: ")
        if len(name) == 0:
            print('Cancelled')
            return
        price = toNumber(input("Preço Inicial: "))

        db = initDB()
        addProduct(name, price, cursor=db.cursor())

        db.commit()
    except Exception as e:
        print(e)

def changePriceHelper(produto, nomeProduto):
    db = initDB()
    price = toNumber(input('Preço Novo: '))
    changePrice(produto, price, cursor=db.cursor())
    db.commit()

    print(f"---------------------------------")
    print(f"  {nomeProduto}({produto}) agora custa {toMoney(price)}")
    print(f"---------------------------------")

def changePriceUI():
    db = initDB()
    print()
    print(f"+++++++++++++++++++++++++++++++++")
    print("Mudar Preço")
    print(f"+++++++++++++++++++++++++++++++++")
    print()

    print(f"---------------------------------")
    print("Produto")
    def factory(x, y):
        produto, nomeProduto = x, y
        return lambda: changePriceHelper(produto, nomeProduto)
    optionsHelper([(row[1], factory(row[0], row[1])) for row in db.execute('SELECT id, name FROM products')])

def buyProductHelperProduct(pessoa, nomePessoa, produto, nomeProduto):
    db = initDB()
    buyProduct(pessoa, produto, cursor=db.cursor())
    db.commit()
    
    print(f"---------------------------------")
    print(f"  {nomePessoa}({pessoa}) comprou {nomeProduto}")
    remainingMoneyHelper(pessoa, nomePessoa)


def buyProductHelper(pessoa, nomePessoa):
    db = initDB()

    print(f"---------------------------------")
    print("Produto")
    def factory(x, y):
        produto, nomeProduto = x, y
        return lambda: buyProductHelperProduct(pessoa, nomePessoa, produto, nomeProduto)
    optionsHelper([(row[1], factory(row[0], row[1])) for row in db.execute('SELECT id, name FROM products')])

def buyProductUI():
    print()
    print(f"+++++++++++++++++++++++++++++++++")
    print("Comprar")
    print(f"+++++++++++++++++++++++++++++++++")
    print()
    db = initDB()

    print(f"---------------------------------")
    print("Pessoa")
    def factory(x, y):
        pessoa, nomePessoa = x, y
        return lambda: buyProductHelper(pessoa, nomePessoa)
    optionsHelper([(row[1], factory(row[0], row[1])) for row in db.execute('SELECT id, name FROM people')])

def depositMoneyHelper(pessoa, nomePessoa):
    db = initDB()
    money = toNumber(input('Quantidade a ser depositada: '))
    depositMoney(pessoa, money, cursor=db.cursor())
    db.commit()

    print(f"---------------------------------")
    print(f"  {nomePessoa}({pessoa}) depositou {toMoney(money)}")
    remainingMoneyHelper(pessoa, nomePessoa)

def depositMoneyUI():
    print()
    print(f"+++++++++++++++++++++++++++++++++")
    print("Depositando Dinheiro")
    print(f"+++++++++++++++++++++++++++++++++")
    print()
    db = initDB()

    print(f"---------------------------------")
    print("Pessoa")
    def factory(x, y):
        pessoa, nomePessoa = x, y
        return lambda: depositMoneyHelper(pessoa, nomePessoa)

    optionsHelper([(str(row[1]), factory(row[0], row[1])) for row in db.execute('SELECT id, name FROM people')])

def remainingMoneyHelper(pessoa, nomePessoa):
    db = initDB()

    saldo = list(db.execute(f"SELECT sum(amount) FROM transactions WHERE person = {pessoa} GROUP BY person"))[0][0]

    print(f"---------------------------------")
    print(f"  {nomePessoa}({pessoa}) tem {toMoney(saldo)}")
    print(f"---------------------------------")

def remainingMoneyUI():
    print()
    print(f"+++++++++++++++++++++++++++++++++")
    print("Saldo")
    print(f"+++++++++++++++++++++++++++++++++")
    print()
    db = initDB()

    print("Pessoa")
    
    def factory(x, y):
        pessoa, nomePessoa = x, y
        return lambda: remainingMoneyHelper(pessoa, nomePessoa)
    optionsHelper([(str(row[1]), factory(row[0], str(row[1]))) for row in db.execute('SELECT id, name FROM people')])

def pricesHelper(produto, nomeProduto):
    db = initDB()

    price = list(db.execute(f"SELECT price FROM products WHERE id = {produto}"))[0][0]

    print(f"---------------------------------")
    print(f"  {nomeProduto}({produto}) custa {toMoney(price)}")
    print(f"---------------------------------")

def pricesUI():
    print()
    print(f"+++++++++++++++++++++++++++++++++")
    print("Depositando Dinheiro")
    print(f"+++++++++++++++++++++++++++++++++")
    print()
    db = initDB()

    print("Produto")
    
    def factory(x, y):
        produto, nomeProduto = x, y
        return lambda: pricesHelper(produto, nomeProduto)
    optionsHelper([(str(row[1]), factory(row[0], str(row[1]))) for row in db.execute('SELECT id, name FROM products')])

def dbQueryUI():
    print()
    print(f"+++++++++++++++++++++++++++++++++")
    print("Depositando Dinheiro")
    print(f"+++++++++++++++++++++++++++++++++")
    print()
    db = initDB()

    query = input('Enter query: ')
    if len(query) == 0:
        print('Cancelled')
        return

    for row in db.executescript(query):
        line = '|'.join(str(i).ljust(21) for i in row)
        print(line)

def mainPage():
    while True:
        print()
        print(f"+++++++++++++++++++++++++++++++++")
        print("Menu Principal")
        print(f"+++++++++++++++++++++++++++++++++")
        print()
        optionsHelper([
            ('Adicionar Pessoa', addPersonUI),
            ('Adicionar Produto', addProductUI),
            ('Mudar Preço Produto', changePriceUI),
            ('Comprar produto', buyProductUI),
            ('Depositar Dinheiro', depositMoneyUI),
            ('Saldos', remainingMoneyUI),
            ('Preços', pricesUI),
            ('Comando DB', dbQueryUI),
        ])


mainPage()
